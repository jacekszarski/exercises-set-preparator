package controllers;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import model.dto.ExerciseDTO;
import model.dto.SavePathDTO;
import org.hibernate.exception.ConstraintViolationException;
import services.ExercisesService;
import services.SavePathService;
import services.impl.ExercisesServiceImpl;
import services.impl.SavePathServiceImpl;
import utils.DialogUtils;
import utils.ExerciseRow;
import utils.ExerciseType;

import javax.imageio.ImageIO;
import javax.persistence.PersistenceException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static utils.StyleUtils.getExerciseLabelStyleClass;
import static utils.StyleUtils.getModificationLabel;

public class MainController implements Initializable {

    private static final String REMOVE_LABEL_STYLE_CLASS = "removeLabel";
    private static final String REMOVE_LABEL_SIGN = "-";
    private static final String EXERCISE_LABEL_COMMON_STYLE_CLASS = "exerciseLabel";
    private static final String AMOUNT_LABEL_STYLE_CLASS = "amountLabel";
    private static final String EDIT_LABEL_STYLE_CLASS = "editLabel";
    private static final String INVISIBLE_CONTROL_STYLE_CLASS = "invisibleControl";
    private static final String ERROR_BORDER_STYLE_CLASS = "errorBorder";
    private static final String EXERCISE_ROW = "exerciseRow";
    private static final String EXERCISE_EDITOR_HEADER = "Edit exercise";
    private static final String EXERCISE_EDITOR_MESSAGE = "Exercise: ";

    @FXML
    private HBox mainPane;

    @FXML
    private VBox exercisesSetListContainer;

    @FXML
    private TextField savePathTextField;

    @FXML
    private Label dragAndDropHintLabel;

    @FXML
    private VBox exercisesSetList;

    @FXML
    private VBox legsExercisesContainer;

    @FXML
    private VBox coreExercisesContainer;

    @FXML
    private VBox otherExercisesContainer;

    @FXML
    private TextField addLegsExercise;

    @FXML
    private TextField addCoreExercise;

    @FXML
    private TextField addOtherExercise;

    private ExerciseType addExerciseType;

    private SavePathDTO savePathDTO;

    private List<ExerciseDTO> exercises;
    private List<ExerciseDTO> setExercises = new ArrayList<>();

    private final ExercisesService exercisesService = new ExercisesServiceImpl();
    private final SavePathService savePathService = new SavePathServiceImpl();

    public void initialize(URL location, ResourceBundle resources) {
        clearFocusFromControls();
        setPath();
        updateExercises();
        setAddExerciseTypeListener();
        setOnDragActionListener();
    }

    private void clearFocusFromControls(){
        Platform.runLater(() -> mainPane.requestFocus());
    }

    private void setPath(){
        if(savePathDTO == null){
            savePathDTO = new SavePathDTO();
        }
        try{
            savePathDTO = new SavePathDTO(savePathService.getPath());
        } catch (Exception e){
            System.out.println();
        }
        savePathTextField.setText(savePathDTO.getPath());
    }

    private void updateExercises(){
        pullExercises();
        updateGUI();
    }

    private void pullExercises(){
        exercises = exercisesService.getAll();
    }

    private void updateGUI(){
        List<ExerciseDTO> legsExercises = getExercisesByType(ExerciseType.LEGS);
        List<ExerciseDTO> coreExercises = getExercisesByType(ExerciseType.CORE);
        List<ExerciseDTO> otherExercises = getExercisesByType(ExerciseType.OTHER);
        fillContainerWithExercises(legsExercisesContainer, legsExercises, addLegsExercise);
        fillContainerWithExercises(coreExercisesContainer, coreExercises, addCoreExercise);
        fillContainerWithExercises(otherExercisesContainer, otherExercises, addOtherExercise);
    }

    private List<ExerciseDTO> getExercisesByType(ExerciseType type){
        return exercises.stream()
                .filter(exerciseDTO -> exerciseDTO.getType().equals(type))
                .collect(Collectors.toList());
    }

    private void fillContainerWithExercises(VBox exercisesContainer, List<ExerciseDTO> exercises, TextField addingField){
        List<HBox> exercisesLabels = exercises.stream()
                .sorted(Comparator.comparing(ExerciseDTO::getUsageAmount)
                        .reversed()
                        .thenComparing(ExerciseDTO::getName))
                .map(this::prepareExerciseRow)
                .collect(Collectors.toList());
        exercisesContainer.getChildren().clear();
        exercisesContainer.getChildren().addAll(exercisesLabels);
        exercisesContainer.getChildren().add(addingField);
    }

    private HBox prepareExerciseRow(ExerciseDTO exerciseDTO){
        Label exerciseLabel = prepareExerciseLabel(exerciseDTO);
        setOnDragDetectedLabelEventListener(exerciseLabel, exerciseDTO.getId(), TransferMode.COPY);

        Label amountLabel = prepareAmountLabel(exerciseDTO);
        Label editLabel = prepareEditLabel(exerciseLabel, exerciseDTO);
        Label removeLabel = prepareRemoveLabel(exerciseDTO.getType());
        HBox exerciseRow = new HBox(exerciseLabel, prepareAnchorPaneWithLabel(amountLabel), prepareAnchorPaneWithLabel(editLabel), prepareAnchorPaneWithLabel(removeLabel));
        exerciseRow.getStyleClass().add(EXERCISE_ROW);
        removeLabel.setOnMouseClicked(event -> {
            removeExercise(new ExerciseRow(exerciseRow, exerciseDTO));
        });
        return exerciseRow;
    }

    private Label prepareExerciseLabel(ExerciseDTO exercise){
        Label label = new Label(exercise.getName());
        label.setWrapText(true);
        label.getStyleClass().add(EXERCISE_LABEL_COMMON_STYLE_CLASS);
        label.getStyleClass().add(getExerciseLabelStyleClass(exercise.getType()));
        return label;
    }

    private void setOnDragDetectedLabelEventListener(Label label, int id, TransferMode mode){
        label.setOnDragDetected(event -> {
            Dragboard dragboard = label.startDragAndDrop(mode);
            ClipboardContent content = new ClipboardContent();
            content.putString(String.valueOf(id));
            dragboard.setContent(content);
            event.consume();
        });
    }

    private Label prepareAmountLabel(ExerciseDTO exerciseDTO){
        Label amountLabel = new Label("[" + exerciseDTO.getUsageAmount() + "]");
        amountLabel.getStyleClass().add(EXERCISE_LABEL_COMMON_STYLE_CLASS);
        amountLabel.getStyleClass().add(getExerciseLabelStyleClass(exerciseDTO.getType()));
        amountLabel.getStyleClass().add(AMOUNT_LABEL_STYLE_CLASS);
        return amountLabel;
    }

    private Label prepareEditLabel(Label exerciseLabel, ExerciseDTO exerciseDTO) {
        Label editLabel = new Label();
        ImageView imageView = new ImageView(new Image(getClass().getResourceAsStream("/images/edit.png")));
        imageView.setFitWidth(10);
        imageView.setFitHeight(10);
        editLabel.setGraphic(imageView);
        editLabel.getStyleClass().add(EXERCISE_LABEL_COMMON_STYLE_CLASS);
        editLabel.getStyleClass().add(getExerciseLabelStyleClass(exerciseDTO.getType()));
        editLabel.getStyleClass().add(getModificationLabel(exerciseDTO.getType()));
        editLabel.getStyleClass().add(EDIT_LABEL_STYLE_CLASS);
        editLabel.setOnMouseClicked(event -> {
            editExercise(exerciseLabel.getText()).ifPresent(value -> {
                try {
                    exerciseDTO.setName(value);
                    exercisesService.update(exerciseDTO.dtoToEntity());
                    exerciseLabel.setText(value);
                } catch (PersistenceException persistenceException){
                    exerciseDTO.setName(exerciseLabel.getText());
                    DialogUtils.showExerciseExistsErrorDialog();
                }
            });
        });
        return editLabel;
    }

    private Optional<String> editExercise(String currentExercise){
        TextInputDialog textInputDialog = new TextInputDialog();
        textInputDialog.setContentText(EXERCISE_EDITOR_MESSAGE);
        textInputDialog.getEditor().setText(currentExercise);
        textInputDialog.setHeaderText(EXERCISE_EDITOR_HEADER);
        return textInputDialog.showAndWait();
    }

    private Label prepareRemoveLabel(ExerciseType type){
        Label removeLabel = new Label(REMOVE_LABEL_SIGN);
        removeLabel.getStyleClass().add(EXERCISE_LABEL_COMMON_STYLE_CLASS);
        removeLabel.getStyleClass().add(getExerciseLabelStyleClass(type));
        removeLabel.getStyleClass().add(REMOVE_LABEL_STYLE_CLASS);
        removeLabel.getStyleClass().add(getModificationLabel(type));
        return removeLabel;
    }

    private AnchorPane prepareAnchorPaneWithLabel(Label label){
        AnchorPane.setTopAnchor(label, 0.0);
        AnchorPane.setLeftAnchor(label, 0.0);
        AnchorPane.setBottomAnchor(label, 0.0);
        AnchorPane.setRightAnchor(label, 0.0);
        return new AnchorPane((label));
    }

    private void removeExercise(ExerciseRow exerciseRow){
        exercisesService.remove(exerciseRow.getExerciseDTO().dtoToEntity());
        exercises.remove(exerciseRow.getExerciseDTO());
        switch (exerciseRow.getExerciseDTO().getType()){
            case OTHER:
                otherExercisesContainer.getChildren().remove(exerciseRow.getRow());
                break;
            case LEGS:
                legsExercisesContainer.getChildren().remove(exerciseRow.getRow());
                break;
            case CORE:
                coreExercisesContainer.getChildren().remove(exerciseRow.getRow());
                break;
        }
    }

    private void setAddExerciseTypeListener(){
        addLegsExercise.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue){
                addExerciseType = ExerciseType.LEGS;
            }
        });
        addCoreExercise.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue){
                addExerciseType = ExerciseType.CORE;
            }
        });
        addOtherExercise.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue){
                addExerciseType = ExerciseType.OTHER;
            }
        });
    }

    private void setOnDragActionListener(){
        exercisesSetListContainer.setOnDragExited(event -> {
            setHintLabelVisibility(true);
        });

        AtomicBoolean added = new AtomicBoolean(false);
        AtomicInteger currentIndex = new AtomicInteger(-1);
        exercisesSetListContainer.setOnDragOver(event -> {
            if (event.getGestureSource() != exercisesSetList && event.getDragboard().hasString()) {
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }
            setHintLabelVisibility(false);

            double dragOnMovePositionY = event.getY();
            double relativeDragOnMovePositionY = dragOnMovePositionY - exercisesSetList.getLayoutY();
            double startListPositionY = exercisesSetList.getLayoutY();;
            if(event.getTransferMode().equals(TransferMode.COPY)) {
                ExerciseDTO exerciseDTO = getExerciseById(event.getDragboard().getString());
                Label label = prepareExerciseLabel(exerciseDTO);
                setOnDragDetectedLabelEventListener(label, exerciseDTO.getId(), TransferMode.MOVE);
                Label removeLabel = prepareRemoveLabel(exerciseDTO.getType());
                HBox exerciseRow = new HBox(label, prepareAnchorPaneWithLabel(removeLabel));
                exerciseRow.getStyleClass().add(EXERCISE_ROW);
                setRemoveExerciseFromSetListener(removeLabel, exerciseDTO, exerciseRow);
                int targetIndex = getTargetIndex(relativeDragOnMovePositionY)
                        .orElse(dragOnMovePositionY < startListPositionY ? 0 : !added.get() ? exercisesSetList.getChildren().size() : exercisesSetList.getChildren().size() -1);
                ExerciseRow row = new ExerciseRow(exerciseRow, exerciseDTO);
                addOrSwapExercises(added, row, currentIndex.get(), targetIndex, startListPositionY);
                currentIndex.set(targetIndex);
            }
            else if(event.getTransferMode().equals(TransferMode.MOVE)) {
                int sourceIndex = exercisesSetList.getChildren().indexOf(((Label)event.getGestureSource()).getParent());
                int targetIndex = getTargetIndex(relativeDragOnMovePositionY)
                        .orElse(dragOnMovePositionY < startListPositionY ? 0 : exercisesSetList.getChildren().size() - 1);
                swapExercises(sourceIndex, targetIndex);
            }
            event.consume();
        });
        exercisesSetListContainer.setOnDragDropped(event -> {
            added.set(false);
        });

    }

    private void setHintLabelVisibility(boolean visible){
        if(!visible){
            exercisesSetList.getChildren().remove(dragAndDropHintLabel);
        } else if (exercisesSetList.getChildren().size() == 0){
            exercisesSetList.getChildren().add(dragAndDropHintLabel);
        }
    }

    private ExerciseDTO getExerciseById(String id){
        return exercises.stream()
                .filter(exercise -> exercise.getId() == Integer.parseInt(id))
                .findFirst().get();
    }

    private void setRemoveExerciseFromSetListener(Label removeLabel, ExerciseDTO exerciseDTO, HBox exerciseRow){
        removeLabel.setOnMouseClicked(mouseEvent -> {
            exercisesSetList.getChildren().remove(exerciseRow);
            setExercises.remove(exerciseDTO);
            setHintLabelVisibility(true);
        });
    }

    private Optional<Integer> getTargetIndex(double relativeEventY){
        return exercisesSetList.getChildren().stream()
                .filter(HBox.class::isInstance)
                .map(HBox.class::cast)
                .filter(label -> label.getLayoutY() <= relativeEventY && label.getLayoutY() + label.getHeight() >= relativeEventY)
                .findFirst()
                .map(label -> exercisesSetList.getChildren().indexOf(label));
    }

    private void addOrSwapExercises(AtomicBoolean added, ExerciseRow exerciseRow, int currentIndex, int targetIndex, double listY){
        if(!added.get()) {
            if (listY < 0) {
                setExercises.add(exerciseRow.getExerciseDTO());
                exercisesSetList.getChildren().add(exerciseRow.getRow());
            } else {
                setExercises.add(targetIndex, exerciseRow.getExerciseDTO());
                exercisesSetList.getChildren().add(targetIndex, exerciseRow.getRow());
            }
            added.set(true);
        } else {
            swapExercises(currentIndex, targetIndex);
        }
    }

    private void swapExercises(int sourceIndex, int targetIndex){
        if(sourceIndex < targetIndex) {
            moveExerciseForward(sourceIndex, targetIndex);
        } else if( sourceIndex > targetIndex){
            moveExerciseBackward(sourceIndex, targetIndex);
        }
    }

    private void moveExerciseForward(int sourceIndex, int targetIndex){
        for (int i = sourceIndex; i < targetIndex; i++) {
            Node node = exercisesSetList.getChildren().get(i + 1);
            ExerciseDTO exerciseDTO = setExercises.get(i+1);
            exercisesSetList.getChildren().remove(i + 1);
            setExercises.remove(i+1);
            exercisesSetList.getChildren().add(i, node);
            setExercises.add(i, exerciseDTO);
        }
    }

    private void moveExerciseBackward(int sourceIndex, int targetIndex) {
        for (int i = sourceIndex; i > targetIndex; i--) {
            Node node = exercisesSetList.getChildren().get(i - 1);
            ExerciseDTO exerciseDTO = setExercises.get(i - 1);
            exercisesSetList.getChildren().remove(i - 1);
            setExercises.remove(i - 1);
            exercisesSetList.getChildren().add(i, node);
            setExercises.add(i, exerciseDTO);
        }
    }

    @FXML
    private void saveExerciseSet(ActionEvent actionEvent) {
        if(isPathSet() && pathExists()) {
            updateUsageAmount();
            setPathError(false);
            hideButtons(true);
            WritableImage image = exercisesSetList.snapshot(null, null);
            hideButtons(false);
            File file = new File(prepareSavingImageName());
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            setPathError(true);
        }
    }

    private boolean isPathSet(){
        return !savePathTextField.getText().isEmpty();
    }

    private boolean pathExists(){
        File file = new File(savePathTextField.getText());
        if(!file.exists()){
            Optional<ButtonType> dialogChoice = DialogUtils.showPathWarning();
            dialogChoice.filter(ButtonType.OK::equals).ifPresent(button -> file.mkdir());
        }
        return file.exists();
    }

    private void updateUsageAmount(){
        setExercises.stream()
                .peek(exerciseDTO -> exerciseDTO.setUsageAmount(exerciseDTO.getUsageAmount() + 1))
                .forEach(exerciseDTO -> exercisesService.update(exerciseDTO.dtoToEntity()));
        updateGUI();
    }

    private void setPathError(boolean isError){
        if(isError){
            savePathTextField.getStyleClass().add(ERROR_BORDER_STYLE_CLASS);
        } else {
            savePathTextField.getStyleClass().remove(ERROR_BORDER_STYLE_CLASS);
        }
    }

    private void hideButtons(boolean hide){
        exercisesSetList.getChildren().stream()
                .filter(HBox.class::isInstance)
                .map(HBox.class::cast)
                .flatMap(hBox -> hBox.getChildren().stream())
                .filter(AnchorPane.class::isInstance)
                .map(AnchorPane.class::cast)
                .forEach(pane -> {
                    if(hide) {
                        pane.getStyleClass().add(INVISIBLE_CONTROL_STYLE_CLASS);
                    }
                    else {
                        pane.getStyleClass().remove(INVISIBLE_CONTROL_STYLE_CLASS);
                    }
                });

    }

    private String prepareSavingImageName(){
        return savePathTextField.getText() + "/exercises_" + getFormattedCurrentTime() + ".png";
    }

    private String getFormattedCurrentTime(){
        String timestamp = new Timestamp(System.currentTimeMillis()).toString();
        timestamp = timestamp.replaceAll("[:]|[.]|[-]","");
        timestamp = timestamp.replaceAll(" ","_");
        return timestamp;
    }

    @FXML
    private void addExercise(ActionEvent actionEvent) {
        TextField exerciseToAddTextField = (TextField) actionEvent.getSource();
        if(!exerciseToAddTextField.getText().isEmpty()) {
            ExerciseDTO exercise = new ExerciseDTO();
            exercise.setName(exerciseToAddTextField.getText());
            exercise.setType(addExerciseType);
            exercise.setUsageAmount(0);
            try {
                int entityId = exercisesService.save(exercise.dtoToEntity());
                exercise.setId(entityId);
                exercises.add(exercise);
                VBox exercisesContainer = (VBox) exerciseToAddTextField.getParent();
                exercisesContainer.getChildren().add(exercisesContainer.getChildren().size() - 1, prepareExerciseRow(exercise));
                exerciseToAddTextField.clear();
            } catch (ConstraintViolationException exception){
                DialogUtils.showExerciseExistsErrorDialog();
            }
        }
    }

    @FXML
    private void browsePath(ActionEvent actionEvent) {
        Optional.ofNullable(new DirectoryChooser().showDialog(exercisesSetList.getScene().getWindow()))
                .map(File::getPath)
                .ifPresent(path -> {
                    setPathError(false);
                    savePathTextField.setText(path);
                });
        savePathDTO.setPath(savePathTextField.getText());
        savePathService.save(savePathDTO.dtoToEntity());
    }

    @FXML
    private void clearExercisesList(ActionEvent actionEvent) {
        exercisesSetList.getChildren().clear();
        setHintLabelVisibility(true);
        setExercises.clear();
    }

    @FXML
    private void openRecent(ActionEvent actionEvent) {
    }
}
