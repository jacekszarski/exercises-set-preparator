import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private static final String CSS_STYLE_PATH = "css/style.css";
    private static final String MAIN_FXML_PATH = "fxml/main.fxml";
    private static final String TITLE = "Interval Exercises";
    private static final int WIDTH = 1000;
    private static final int HEIGHT = 600;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource(MAIN_FXML_PATH));
        root.getStylesheets().add(CSS_STYLE_PATH);
        primaryStage.setTitle(TITLE);
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}