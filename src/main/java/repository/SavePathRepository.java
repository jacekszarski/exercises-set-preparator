package repository;

import model.entities.SavePath;

public interface SavePathRepository {

    void save(SavePath savePath);

    SavePath getPath();
}
