package repository;

import model.dto.ExerciseDTO;
import model.entities.Exercise;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import utils.ExerciseType;

import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public interface ExercisesRepository {

    int save(Exercise exercise) throws ConstraintViolationException;

    void update(Exercise exercise) throws PersistenceException;

    List<Exercise> getAll();

    void remove(Exercise exercise);
}
