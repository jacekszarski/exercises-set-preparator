package repository.impl;

import db.HibernateUtil;
import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;
import model.dto.ExerciseDTO;
import model.entities.Exercise;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import repository.ExercisesRepository;
import utils.DialogUtils;
import utils.ExerciseType;

import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.awt.*;
import java.util.List;
import java.util.Optional;

public class ExercisesRepositoryImpl implements ExercisesRepository {

    private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public int save(Exercise exercise) throws ConstraintViolationException{
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            int id = (int) session.save(exercise);
            transaction.commit();
            return id;
        } catch (ConstraintViolationException exception) {
            throw exception;
        }
    }

    @Override
    public void update(Exercise exercise) throws PersistenceException{
        Transaction transaction = null;
        try(Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(exercise);
            transaction.commit();
        } catch (PersistenceException exception){
            throw exception;
        }
    }

    @Override
    public List<Exercise> getAll(){
        Session session = sessionFactory.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Exercise> criteriaQuery = criteriaBuilder.createQuery(Exercise.class);
        Root<Exercise> rootEntry = criteriaQuery.from(Exercise.class);
        CriteriaQuery<Exercise> allByType = criteriaQuery.select(rootEntry);
        return session.createQuery(allByType).getResultList();
    }

    @Override
    public void remove(Exercise exercise) {
        Transaction transaction = null;
        try(Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.delete(exercise);
            transaction.commit();
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }
}
