package repository.impl;

import db.HibernateUtil;
import model.entities.SavePath;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import repository.SavePathRepository;

import java.io.Serializable;

public class SavePathRepositoryImpl implements SavePathRepository {

    private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public void save(SavePath savePath){
        Transaction transaction = null;
        try(Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.saveOrUpdate(savePath);
            transaction.commit();
        } catch (Exception e){
            if(transaction != null){
                transaction.rollback();
            }
        }
    }

    @Override
    public SavePath getPath(){
        Session session = sessionFactory.openSession();
        return session.get(SavePath.class, 1);
    }
}
