package utils;

public class StyleUtils {

    private static final String OTHER_LABEL_STYLE_CLASS = "otherLabel";
    private static final String LEGS_LABEL_STYLE_CLASS = "legsLabel";
    private static final String CORE_LABEL_STYLE_CLASS = "coreLabel";
    private static final String LEGS_MODIFICATION_LABEL_STYLE_CLASS = "legsModificationLabel";
    private static final String CORE_MODIFICATION_LABEL_STYLE_CLASS = "coreModificationLabel";
    private static final String OTHER_MODIFICATION_LABEL_STYLE_CLASS = "otherModificationLabel";
    private static final String EMPTY_LABEL_STYLE_CLASS = "";

    public static String getExerciseLabelStyleClass(ExerciseType exerciseType){
        switch (exerciseType){
            case OTHER:
                return OTHER_LABEL_STYLE_CLASS;
            case LEGS:
                return LEGS_LABEL_STYLE_CLASS;
            case CORE:
                return CORE_LABEL_STYLE_CLASS;
            default:
                return EMPTY_LABEL_STYLE_CLASS;
        }
    }

    public static String getModificationLabel(ExerciseType type){
        switch (type){
            case LEGS:
                return LEGS_MODIFICATION_LABEL_STYLE_CLASS;
            case CORE:
                return CORE_MODIFICATION_LABEL_STYLE_CLASS;
            case OTHER:
                return OTHER_MODIFICATION_LABEL_STYLE_CLASS;
            default:
                return EMPTY_LABEL_STYLE_CLASS;
        }
    }
}
