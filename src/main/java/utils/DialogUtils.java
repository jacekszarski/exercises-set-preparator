package utils;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class DialogUtils {

    private static final String ERROR_TITLE = "Error";
    private static final String EXERCISE_EXISTS = "Provided exercise already esists!";
    private static final String PATH_NOT_EXISTS = "Path does not exists.";
    private static final String PATH_CREATION_CONFIRMATION = "Create?";

    public static void showExerciseExistsErrorDialog(){
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setHeaderText(ERROR_TITLE);
        dialog.setContentText(EXERCISE_EXISTS);
        dialog.show();
    }

    public static Optional<ButtonType> showPathWarning(){
        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setHeaderText(PATH_NOT_EXISTS);
        dialog.setContentText(PATH_CREATION_CONFIRMATION);
        return dialog.showAndWait();
    }
}
