package utils;

import javafx.scene.layout.HBox;
import model.dto.ExerciseDTO;

public class ExerciseRow {

    private HBox row;

    private ExerciseDTO exerciseDTO;

    public ExerciseRow(HBox row, ExerciseDTO exerciseDTO) {
        this.row = row;
        this.exerciseDTO = exerciseDTO;
    }

    public HBox getRow() {
        return row;
    }

    public void setRow(HBox row) {
        this.row = row;
    }

    public ExerciseDTO getExerciseDTO() {
        return exerciseDTO;
    }

    public void setExerciseDTO(ExerciseDTO exerciseDTO) {
        this.exerciseDTO = exerciseDTO;
    }
}
