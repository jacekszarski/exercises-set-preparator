package model.dto;

import model.entities.Exercise;
import utils.ExerciseType;

public class ExerciseDTO {

    private int id = -1;

    private String name;

    private ExerciseType type;

    private int usageAmount;

    public ExerciseDTO(){

    }
    public ExerciseDTO(Exercise exercise){
        id = exercise.getId();
        name = exercise.getName();
        type = exercise.getType();
        usageAmount = exercise.getUsageAmount();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ExerciseType getType() {
        return type;
    }

    public void setType(ExerciseType type) {
        this.type = type;
    }

    public int getUsageAmount() {
        return usageAmount;
    }

    public void setUsageAmount(int usageAmount) {
        this.usageAmount = usageAmount;
    }

    public Exercise dtoToEntity(){
        Exercise exercise = new Exercise();
        if(id != -1){
            exercise.setId(id);
        }
        exercise.setName(name);
        exercise.setType(type);
        exercise.setUsageAmount(usageAmount);
        return exercise;
    }

//    @Override
//    public boolean equals(Object obj) {
//        if(!(obj instanceof ExerciseDTO)){
//            return false;
//        }
//        return ((ExerciseDTO) obj).getId() == id;
//    }
}
