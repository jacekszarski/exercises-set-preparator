package model.dto;

import model.entities.SavePath;

public class SavePathDTO {

    private int id;

    private String path;

    public SavePathDTO(){
        id = -1;
        path = "";
    }

    public SavePathDTO(SavePath savePath){
        id = savePath.getId();
        path = savePath.getPath();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public SavePath dtoToEntity(){
        SavePath savePath = new SavePath();
        if(id != -1) {
            savePath.setId(id);
        }
        savePath.setPath(path);
        return savePath;
    }
}
