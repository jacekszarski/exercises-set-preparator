package services.impl;

import model.entities.SavePath;
import repository.SavePathRepository;
import repository.impl.SavePathRepositoryImpl;
import services.SavePathService;

public class SavePathServiceImpl implements SavePathService {

    private final SavePathRepository repository = new SavePathRepositoryImpl();

    @Override
    public void save(SavePath savePath){
        repository.save(savePath);
    }

    @Override
    public SavePath getPath(){
        return repository.getPath();
    }
}
