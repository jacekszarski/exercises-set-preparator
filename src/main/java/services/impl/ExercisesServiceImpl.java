package services.impl;

import model.dto.ExerciseDTO;
import model.entities.Exercise;
import org.hibernate.exception.ConstraintViolationException;
import repository.ExercisesRepository;
import repository.impl.ExercisesRepositoryImpl;
import services.ExercisesService;
import utils.ExerciseType;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.stream.Collectors;

public class ExercisesServiceImpl implements ExercisesService {

    private final ExercisesRepository repository = new ExercisesRepositoryImpl();

    @Override
    public int save(Exercise exercise) throws ConstraintViolationException {
        return repository.save(exercise);
    }

    @Override
    public void update(Exercise exercise) throws PersistenceException {
        repository.update(exercise);
    }

    @Override
    public List<ExerciseDTO> getAll(){
        return repository.getAll().stream()
                .map(ExerciseDTO::new)
                .collect(Collectors.toList());
    }

    @Override
    public void remove(Exercise exercise) {
        repository.remove(exercise);
    }
}
