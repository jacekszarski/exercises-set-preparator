package services;

import model.dto.ExerciseDTO;
import model.entities.Exercise;
import org.hibernate.exception.ConstraintViolationException;
import utils.ExerciseType;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.stream.Collectors;

public interface ExercisesService {

    int save(Exercise exercise) throws ConstraintViolationException;

    void update(Exercise exercise) throws PersistenceException;

    List<ExerciseDTO> getAll();

    void remove(Exercise exercise);
}
