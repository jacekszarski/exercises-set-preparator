package services;

import model.entities.SavePath;

public interface SavePathService {

    void save(SavePath savePath);

    SavePath getPath();
}
